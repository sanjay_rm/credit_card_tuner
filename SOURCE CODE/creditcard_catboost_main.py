import pandas as pd
from sklearn.model_selection import train_test_split
import time
import numpy as np
import matplotlib.pyplot as plt
from catboost import CatBoostClassifier
from sklearn import metrics
from sklearn.metrics import mean_squared_error

df=pd.read_csv("/home/sanjay/Desktop/sets/15updated.csv")
features=['V1','V2','V3','V4','V5','V6','V7','V8','V9','V10','V11','V12','V13','V14','V15','V16',
'V17','V18','V19','V20','V21','V22','V23','V24','V25','V26','V27','V28','Amount']
X=df[features]
y=df['Class']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=23, shuffle='False')

cb_model=CatBoostClassifier(iterations=700, learning_rate=0.0001,
                                       max_depth=10,
                                       leaf_estimation_iterations=5,
                                       border_count=32,
                                       fold_permutation_block=10)

cb_model.fit(X_train, y_train,
             use_best_model=True,
             verbose=True)

y_pred_class=cb_model.predict(X_test)
y_pred_prob=cb_model.predict_proba(X_test)[:,1]

fpr,tpr,thresholds=metrics.roc_curve(y_test,y_pred_prob)
cb_model.save_model("/home/sanjay/Desktop/models/tuning5",format="cbm")
auc_score=metrics.roc_auc_score(y_test,y_pred_prob)
rounded=round(auc_score,3)
textonplot="AUC score: "+str(rounded)

plt.plot(fpr,tpr)
plt.xlim([0.0,1.0])
plt.ylim([0.0,1.0])
plt.title("ROC Curve")
plt.xlabel("FPR")
plt.ylabel("TPR")
plt.grid(True)
plt.text(0.5,0.5,textonplot)
plt.savefig('/home/sanjay/Desktop/ROC_Curve_Tuning_Stage_5.png')
plt.show()




